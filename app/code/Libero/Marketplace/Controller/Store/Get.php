<?php
namespace Libero\Marketplace\Controller\Store;

use Magento\Framework\App\Action\Context;

class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_urlBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->_urlBuilder = $context->getUrl();
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelCustomer = $objectManager->create('Libero\Customer\Model\CustomerSellerStore');
        $collection = $modelCustomer->getCollection();
        $array_store = array();
        foreach($collection as $_store){
            $array_item = array();
            $array_item["link"] = $_store->getData("frontend_url");
            $array_item["link_image"] = "http://devlibero.com/pub/media/wysiwyg/gift-bank.jpg";
            $array_item["biz_type"] = $_store->getData("name_store");
            $array_store[] = $array_item;
        }
        //$stores = array("stores_seller" => $array_store);
        echo json_encode($array_store);
        return;
    }
}
