<?php
namespace Libero\Marketplace\Block;

use Magento\Framework\View\Element\Template;

class Seller extends \Magento\Framework\View\Element\Template{


    //Using dependen injecttion class

    protected $layoutProcessors;

    public function __construct(
        Template\Context $context,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->layoutProcessors = $layoutProcessors;
    }

    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return \Zend_Json::encode($this->jsLayout);
    }

    public function getSellerCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelCustomer = $objectManager->create('Libero\Customer\Model\CustomerSellerStore');
        $collection = $modelCustomer->getCollection();
        return $collection;
    }

    public function filterSellerByCategory($id_category){

    }

}