<?php
namespace Libero\Onestepcheckout\Controller\Ajax;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $toCity = $postData["toCity"];
            $toProvince = $postData["toProvince"];
            $priceProduct = $postData["price_product"];
            $cartPrice = intval($priceProduct);
            $weight = $postData["weight"];
            $arrayPostData  = array(
                "From" => array("City"  =>  52,"Province"   => 569),
                "To" => array("City"    =>  $toCity,"Province" => $toProvince),
                "Order" =>  array("Amount"   =>  $cartPrice,"Weight" =>  $weight),
                "Config"    =>  array("Service" =>  2,"CoD" =>  1,"Protected"   =>  1,"Checking"    =>  1,"Payment" =>  2,"Fragile" =>  2),
                "Domain" =>  "haravan.com",
                "MerchantKey" =>"b7dfd7457c04b8fd547068bfda886681"
            );
            //Use CURL to get
            $resultJson = $this->callUrl("http://prod.boxme.vn/api/public/api/rest/courier/calculate",$arrayPostData);
            //End
            $resultJson = json_decode($resultJson);
            //$result = $this->resultPageFactory->create();
            $result = $jsonResultFactory->setData($resultJson);
            return $result;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
    public function callUrl($url,$postData){
        $postData=json_encode($postData);
        $header=array(
            "Accept:application/json",
            "Content-Type:application/json"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
