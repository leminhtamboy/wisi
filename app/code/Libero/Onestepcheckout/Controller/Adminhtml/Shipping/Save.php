<?php
namespace Libero\Onestepcheckout\Controller\Adminhtml\Shipping;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Save extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultPage = $this->resultPageFactory->create();
            $post = $this->getRequest()->getPostValue();
            $name_shipping = $post["valueName"];
            //Create
            $modelShipping = $this->_objectManager->create("Libero\Onestepcheckout\Model\Shipping");
            $checkHaveShipping = $modelShipping->getCollection()->addFieldToFilter("name_shipping_method",array("eq" => $name_shipping));
            if(count($checkHaveShipping) > 0){
                $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
                $result = array("mess" => "haved");
                $resultJson = json_encode($result);
                $jsonResultFactory->setData($result);
                return $jsonResultFactory;
            }else {
                $modelShipping->setData("name_shipping_method", $name_shipping);
                $modelShipping->setData("title_shipping_method", $name_shipping);
                $modelShipping->setData("status_shipping_method", 1);
                $modelShipping->save();
                return $resultPage;
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
}