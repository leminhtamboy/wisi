<?php
namespace Libero\Onestepcheckout\Controller\Index;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        /*$data = $this->getRequest()->getParam("discount_code");
        if($data==""){
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('onestepcheckout/success/index');
        }*/
        $modelCheckoutSession = $this->_objectManager->create("\Magento\Checkout\Model\Session");
        $quote = $modelCheckoutSession->getQuote();
        $items = $quote->getAllItems();
        if(count($items) < 1){
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('onestepcheckout/success/index');
        }
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
