<?php
namespace Libero\Onestepcheckout\Controller\Ordercheckout;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Place extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_helperOneStep;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Libero\Onestepcheckout\Helper\Data $helperOneStep
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_helperOneStep = $helperOneStep;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $postData = $this->getRequest()->getPostValue();
            $payment_code = $postData["code_payment"];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerModel = $objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
            $customer = $customerModel->getCustomer();
            $customer_id  = $customer->getId();
            if($payment_code == "custompayment"){
                $block = $objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
                $quote = $block->getCheckoutSession()->getQuote();
                $customerObj = $objectManager->create('Magento\Customer\Model\Customer');
                $modelCustomer = $customerObj->load($customer_id);
                $balance = $modelCustomer->getData("virtual_money");
                $grand_total = floor($quote->getData("grand_total"));
                if($grand_total >  $balance){
                    $this->messageManager->addError(__("Your credit money does not enough to pay."));
                    $result = json_encode(['error'=>1,'msg'=>'Your credit money does not enough to pay']);
                    $resultJson = $jsonResultFactory->setData($result);
                    return $resultJson;
                }else {
                    $price = $grand_total;
                    $money = $balance - $price;
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $connection = $resource->getConnection();
                    $sql = "UPDATE `customer_entity` SET `first_failure` = NULL, `lock_expires` = NULL, `virtual_money` = '" . $money . "' WHERE `customer_entity`.`entity_id` = $customer_id;";
                    $connection->query($sql);
                }
            }
            $result = $this->_helperOneStep->createMageOrder($payment_code);
            $registry = $this->_objectManager->create("\Magento\Checkout\Model\Session");
            $registry->setOrderIncreaId($result["order_id"]);
            $resultJson = $jsonResultFactory->setData(json_encode($result));
            return $resultJson;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
