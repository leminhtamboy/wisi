<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class Payment extends \Libero\Onestepcheckout\Block\AbstractCheckOut{

    protected $_customer = null;

    protected $_paymentMethod = null;

    public function __construct(Template\Context $context,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Libero\Onestepcheckout\Model\Paymentmethod $paymentModel,
                                array $data = []){

        parent::__construct($context,$cart,$checkoutSession,$data);
        if(parent::getCustomerIfLogin()) {
            $customerAbstract = parent::getCustomer();
            $this->_customer = $customerAbstract;
            //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //$customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($this->_customer->getId());
            $this->_paymentMethod = $paymentModel;
        }
    }

    public function getPaymentMethodList(){
        $listPayments = $this->_paymentMethod->toOptionArray();
        return $listPayments;
    }
    public function getPostUrlPlaceOrder(){
        return $this->getUrl("onestepcheckout/ordercheckout/place");
    }
    public function getPostUrlGetShippingMethod(){
        return $this->getUrl("onestepcheckout/ajax/get");
    }
    public function getSuccessUrl(){
        return $this->getUrl("onestepcheckout/success/index");
    }
}