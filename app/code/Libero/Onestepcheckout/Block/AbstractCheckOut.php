<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class AbstractCheckOut extends \Magento\Framework\View\Element\Template{

    protected $_cart;
    protected $_checkoutSession;

    public function __construct(
        Template\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = [])
    {
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
    }
    public function getCustomerIfLogin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            // customer login action
            return true;
        }
        return false;
    }
    public function getCustomer(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerModel = $objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
        return $customerModel->getCustomer();
    }

    public function getCart()
    {
        return $this->_cart;
    }
    public function getQuoteCheckout(){
        return $this->_cart->getQuote();
    }
    public function getCheckoutSession(){

        return $this->_checkoutSession;
    }
    public function getOrderByIncreasementId($increaseId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderModel = $objectManager->get('Magento\Sales\Model\Order');
        $orderData = $orderModel->loadByIncrementId($increaseId);
        return $orderData;
    }
    public function getHomeUrl(){
        return $this->getBaseUrl();
    }
    public function getOrderDetail($orderId){
        return $this->getUrl("sales/order/view/order_id/$orderId/");
    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
    /**
     * @param  [array] id product
     * @return [type] collection products
     */
    public function getRelatedProducts($order){

        $items = $order->getAllItems();
        $arrayRelatedProducts = array();
        die("something");
        foreach($items as $_item){
            $parent_id = $_item->getData("parent_item_id");
            if($parent_id)
                continue;
            //$id = $_item->getData("product_id");
            $product = $_item->getProduct()->getRelatedProducts();
            $arrayRelatedProducts[] = $product;
        }

        return $arrayRelatedProducts;
    }
}