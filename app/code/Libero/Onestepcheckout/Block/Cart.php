<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class Cart extends \Libero\Onestepcheckout\Block\AbstractCheckOut{

    protected $_customer = null;

    protected $_billingAddress = null;

    protected $_shippingAddress = null;

    protected $_productImageHelper = null;
    public function __construct(Template\Context $context,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Catalog\Helper\Image $productImageHelper,
                                array $data = []){
        $this->_productImageHelper = $productImageHelper;
        parent::__construct($context,$cart,$checkoutSession,$data);
        if(parent::getCustomerIfLogin()) {
            $customerAbstract = parent::getCustomer();
            $this->_customer = $customerAbstract;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($this->_customer->getId());
            $this->_billingAddress = $customerObj->getDefaultBillingAddress();
            $this->_shippingAddress = $customerObj->getDefaultShippingAddress();
        }
    }

    public function getCustomerData(){

        return $this->_customer;
    }
    public function getBillingAddress(){

        return $this->_billingAddress;
    }
    public function getShippingAddress(){

        return $this->_shippingAddress;
    }
    public function getArrayAddress($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $customerAddress = array();
        foreach ($customerObj->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }
        //testing
        /*foreach ($customerAddress as $customerAddres) {
            echo $customerAddres['street'];
            echo $customerAddres['city'];
        }*/
        return $customerAddress;
    }
    public function getAddressOfCustomer(){

    }
    public function getPostUrlSaveAddress(){
        return $this->getUrl("onestepcheckout/address/save");
    }
    public function getPostUrlGetShippingMethod(){
        return $this->getUrl("onestepcheckout/ajax/get");
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }
}