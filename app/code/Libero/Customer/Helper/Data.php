<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SocialLogin
 * @copyright   Copyright (c) 2016 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */
namespace Libero\Customer\Helper;


use Magento\Framework\App\Helper\AbstractHelper as CoreHelper;
use Magento\Store\Model\ScopeInterface;
/**
 * Class Data
 *
 * @package Libero\Customer\Helper
 */
class Data extends CoreHelper
{
    /**
     * @type
     */
    protected $_type;

    public function getBaseDir($store_name){
        $rootWeb = $_SERVER['DOCUMENT_ROOT'];
        $baseDir = $rootWeb."/seller/".$store_name;
        return $baseDir;
    }

}