<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Import extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {

            $resultRedirect = $this->resultRedirectFactory->create();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $coreConfig = $objectManager->get("\Magento\Framework\App\Config\ScopeConfigInterface");
            $postData = $this->getRequest()->getPostValue();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $storeUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $store_name = $postData["store_name"];
            $url_rewrite = $store_name;
            $helperSeller = $objectManager->get("\Libero\Customer\Helper\Data");
            $store_name = str_replace("-","_",$store_name);
            $arrayResult = array(
                "error" => false,
                "message"=> ""
            );
            $server  =  'localhost';
            $username   = $coreConfig->getValue("config_seller/general/user_name",\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $password   = $coreConfig->getValue("config_seller/general/password",\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $database = $store_name;
            $link = mysqli_connect('localhost', $username,$password);
            if (!$link) {
                $arrayResult = array(
                    "error" => true,
                    "message"=> "Could not connect"
                );
            }
            $sql = 'CREATE DATABASE '.$store_name;
            if (mysqli_query($link,$sql)) {
                $arrayResult = array(
                    "error" => false,
                    "message"=> "Database $store_name created successfully"
                );
            } else {
                $arrayResult = array(
                    "error" => false,
                    "message"=> "Error creating database:"
                );
            }
            $rootWeb = $_SERVER['DOCUMENT_ROOT'];
            //Export the database and output the status to the page

            $conn = new \PDO("mysql:host=$server; dbname=$database", $username, $password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $conn->exec("SET CHARACTER SET utf8");
            $conn->exec("SET FOREIGN_KEY_CHECKS=0");
            // your config
            $filename = $rootWeb.'/libero_seller.sql';
            $data = file_get_contents($filename);
            $conn->exec($data);
            /*$templine = "";
            $lines = file($filename);
            // Loop through each line
            foreach ($lines as $line){
            // Skip it if it's a comment
                if (substr($line, 0, 2) == '--' || $line == '') {
                    continue;
                }
            // Add this line to the current segment
                $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
                if (substr(trim($line), -1, 1) == ';')
                {
                    // Perform the query
                    try {
                        $conn->exec($templine);
                    }catch(Exception $e){
                        //or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                        echo 'Error performing query \'<strong>' . $templine . '\': ' . $e->getMessage() . '<br /><br />';
                    }
                    // Reset temp variable to empty
                    $templine = '';
                }
            }*/
            $conn->exec("SET FOREIGN_KEY_CHECKS=1");
            $domain = $storeUrl."seller/$url_rewrite/";
            $sql_update ="update core_config_data set `value` = '$domain' where path like 'web/unsecure/base_url'";
            $conn->exec($sql_update);
            $sql_update ="update core_config_data set `value` = '$domain' where path like 'web/secure/base_url'";
            $conn->exec($sql_update);
            $sql_update ="update core_config_data set `value` = 'UTC' where path like 'general/locale/timezone'";
            $conn->exec($sql_update);
            $sql_update ="update core_config_data set `value` = '".$url_rewrite."' where path like 'design/header/welcome'";
            $conn->exec($sql_update);
            //Change DI File
            $dataFile="<?php
            return array (
              'backend' => 
              array (
                'frontName' => 'admin',
              ),
              'crypt' => 
              array (
                'key' => '442fe391356c3bb56764795a46f13b7f',
              ),
              'session' => 
              array (
                'save' => 'files',
              ),
              'db' => 
              array (
                'table_prefix' => '',
                'connection' => 
                array (
                  'default' => 
                  array (
                    'host' => 'localhost',
                    'dbname' => '".$store_name."',
                    'username' => '".$username."',
                    'password' => '".$password."',
                    'active' => '1',
                  ),
                ),
              ),
              'resource' => 
              array (
                'default_setup' => 
                array (
                  'connection' => 'default',
                ),
              ),
              'x-frame-options' => 'SAMEORIGIN',
              'MAGE_MODE' => 'default',
              'cache_types' => 
              array (
                'config' => 1,
                'layout' => 1,
                'block_html' => 1,
                'collections' => 1,
                'reflection' => 1,
                'db_ddl' => 1,
                'eav' => 1,
                'customer_notification' => 1,
                'full_page' => 1,
                'config_integration' => 1,
                'config_integration_api' => 1,
                'translate' => 1,
                'config_webservice' => 1,
                'compiled_config' => 1,
              ),
              'install' => 
              array (
                'date' => 'Mon, 15 Jan 2018 08:44:45 +0000',
              ),
            );
            ";
            $baseDir = $helperSeller->getBaseDir($url_rewrite);
            $fileETC = file_put_contents($baseDir."/app/etc/env.php",$dataFile);
            //end import and change product now
            $resultJson = json_encode($arrayResult);
            $jsonResultFactory->setData($resultJson);
            return $jsonResultFactory;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*');
        }
    }
}
