<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(


        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $phone = $postData["phone"];
            $email = $postData["email"];
            $sellerName = $postData["seller_name"];
            $password = $postData["password"];
            $passwordConfirm = $postData["password_2"];
            if ($password != $passwordConfirm) {
                $this->messageManager->addError(__("Password confirm does not match"));
                return $resultRedirect->setPath("*/*");
            }
            $hot_line = $postData["hot_line"];
            $store_name = $postData["store_name"];
            //Company information
            $biz_type = $postData["biz_type"];
            $company = $postData["company"];
            $address = $postData["address"];
            $city = $postData["city"];
            $district = $postData["district"];
            $street = $postData["street"];
            $tax = $postData["tax"];
            $home_page = $postData["homepage"];
            $agent_name = $postData["agent_name"];
            $registration_number = $postData["registration_number"];
            $bank_name = $postData["bank_name"];
            $bank_account = $postData["bank_account"];
            $etc = $postData["etc"];
            $is_manufacture = $postData["manufacture"];
            $is_genuine = $postData["genuine"];
            //$dateOfBirth = $postData["yy"] . "-" . $postData["mm"] . "-" . $postData["dd"];

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $url = \Magento\Framework\App\ObjectManager::getInstance();
            //Upload image
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader = $fileUploaderFactory->create(['fileId' => 'certification']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('upload_manager/'.$sellerName."/".$email."/".date("d-m-Y"));
			$pathSave = 'upload_manager/'.$sellerName."/".$email."/".date("d-m-Y");
            $result = $uploader->save($path);
            $imagepath_1= $pathSave."/".$result['file'];
            //
            $uploader_1 = $fileUploaderFactory->create(['fileId' => 'certification_1']);
            $uploader_1->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
            $uploader_1->setAllowRenameFiles(false);
            $uploader_1->setFilesDispersion(false);
            $result_1 = $uploader_1->save($path);
            $imagepath_2 = $pathSave."/".$result_1["file"];
            //
            $uploader_2 = $fileUploaderFactory->create(['fileId' => 'certification_2']);
            $uploader_2->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
            $uploader_2->setAllowRenameFiles(false);
            $uploader_2->setFilesDispersion(false);
            $result_2 = $uploader_2->save($path);
            $imagepath_3 = $pathSave."/".$result_2["file"];
            //End upload
            $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');
            $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
            $websiteId = $storeManager->getWebsite()->getWebsiteId();
            $store = $storeManager->getStore();  // Get Store ID
            $storeId = $store->getStoreId();
            $modelCustomerSellerCompany = $this->_objectManager->create("\Libero\Customer\Model\CustomerSeller");
            $modelCustomerSellerCompany->setData("company",$company);
            $modelCustomerSellerCompany->setData("address",$address);
            $modelCustomerSellerCompany->setData("city",$city);
            $modelCustomerSellerCompany->setData("district",$district);
            $modelCustomerSellerCompany->setData("street",$street);
            $modelCustomerSellerCompany->setData("tax",$tax);
            $modelCustomerSellerCompany->setData("home_page",$home_page);
            $modelCustomerSellerCompany->setData("agent_name",$agent_name);
            $modelCustomerSellerCompany->setData("bank_name",$bank_name);
            $modelCustomerSellerCompany->setData("bank_account",$bank_account);
            $modelCustomerSellerCompany->setData("etc",$etc);
            $modelCustomerSellerCompany->setData("is_manufacture",$is_manufacture);
            $modelCustomerSellerCompany->setData("is_genuine",$is_genuine);
            $modelCustomerSellerCompany->setData("biz_type",$biz_type);
            $modelCustomerSellerCompany->setData("company_registration",$registration_number);
            $modelCustomerSellerCompany->setData("up_load_cer_1",$imagepath_1);
            $modelCustomerSellerCompany->setData("up_load_cer_2",$imagepath_2);
            $modelCustomerSellerCompany->setData("up_load_cer_3",$imagepath_3);
            $modelCustomerSellerCompany->setData("store_name",$store_name);
            $modelCustomerSellerCompany->setData("status",0);
            $modelCustomerSellerCompany->setData("message_denied","");
            //Save customer
            $customer = $customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname($sellerName);
            $customer->setLastname("");
            $customer->setPassword($password);
            //$customer->setDob($dateOfBirth);
            $customer->setGroupId(2);
            $customer->save();
            $postCode = $postData["city_app"]."-".$postData["province"];
            //Save address customer
            $addresssModel = $this->_objectManager->get('\Magento\Customer\Model\AddressFactory');
            $addressModel = $addresssModel->create();
            $addressModel->setCustomerId($customer->getId())
                ->setFirstname($sellerName)
                ->setLastname('&nbsp')
                ->setCountryId('VN')
                ->setPostcode($postCode)
                ->setCity($city)
                ->setTelephone($phone)
                ->setFax($tax)
                ->setCompany($company)
                ->setCompany($company)
                ->setStreet($address." ".$street." ".$district)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');
            $addressModel->save();
			$modelCustomerSellerCompany->setData("id_customer",$customer->getId());
			$modelCustomerSellerCompany->save();
            if ($customer && $customer->getId()) {
                $session  = $objectManager->create("\Magento\Customer\Model\Session");
                $session->setCustomerAsLoggedIn($customer);
                $session->regenerateId();

                /*if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                    $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                }*/

            }
            $this->messageManager->addSuccess(__("Create Seller Successfully"));
            $resultRedirect->setPath('customer/account');
            return $resultRedirect;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*');
        }
    }
}
