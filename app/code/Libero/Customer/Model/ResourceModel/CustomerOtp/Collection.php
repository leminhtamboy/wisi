<?php
namespace Libero\Customer\Model\ResourceModel\CustomerOtp;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\CustomerOtp',
            'Libero\Customer\Model\ResourceModel\CustomerOtp'
        );
    }
}