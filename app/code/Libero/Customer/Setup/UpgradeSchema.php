<?php
namespace Libero\Customer\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            //code to upgrade to 1.0.1
            $setup->run("CREATE TABLE `libero_customer_seller_company` ( `id_seller_company` INT NOT NULL AUTO_INCREMENT , `company` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `address` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `city` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `district` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `street` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `tax` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `home_page` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `agent_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `bank_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `bank_account` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `etc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `is_manufacture` INT NOT NULL , `is_genuine` INT NOT NULL , `biz_type` INT NOT NULL , `company_registration` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_seller_company`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->run("CREATE TABLE `libero_customer_social_login` ( `social_id` INT NOT NULL AUTO_INCREMENT , `customer_id` VARCHAR(50) NOT NULL , `type` VARCHAR(50) NOT NULL , `is_send_pass` INT NOT NULL , PRIMARY KEY (`social_id`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `up_load_cer_1` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `company_registration`, ADD `up_load_cer_2` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_1`, ADD `up_load_cer_3` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_2`;");
		}
		if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `id_customer` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_3`;");
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `status` INT NOT NULL AFTER `id_customer`, ADD `message_denied` TEXT NOT NULL AFTER `status`");
        }
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `store_name` VARCHAR(250) NOT NULL AFTER `message_denied`;");
        }
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $setup->run("CREATE TABLE `libero_customer_seller_store` ( `id_store` INT NOT NULL AUTO_INCREMENT , `id_seller` INT NOT NULL , `sku_store` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `name_store` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `frontend_url` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `backend_url` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `user_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `password` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `alias` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_store`)) ENGINE = InnoDB;");
        }
        //remove last name required
        if(version_compare($context->getVersion(),'1.0.9') < 0){
            $setup->run("UPDATE eav_attribute SET is_required = 0 WHERE attribute_code = 'lastname' and entity_type_id = 1;");
        }
        $setup->endSetup();
    }
}