<?php
namespace Libero\Customer\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements  InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //Do something
        $setup->getConnection();
        $setup->run("CREATE TABLE `libero_customer_otp` ( `id_code` INT NOT NULL AUTO_INCREMENT , `token` VARCHAR(50) NOT NULL , `otp` VARCHAR(50) NOT NULL , `phone` VARCHAR(50) NOT NULL , PRIMARY KEY (`id_code`)) ENGINE = InnoDB;");
        $setup->endSetup();
    }
}
