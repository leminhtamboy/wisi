<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class View extends \Magento\Framework\View\Element\Template{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function sendSms(){

    }
    public function sendEmail(){

    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
    public function getPostActionGetOtp(){
        return $this->getUrl("customer/seller/getcode");
    }
    public function randomTokenonFormToSendOtp(){
        //Get Model random
        $stringRamdon = "Token:".rand(1,999999);
        return base64_encode($stringRamdon);
    }

}