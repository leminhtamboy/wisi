<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Override\Weee\Block\Item\Price;

/**
 * Item price render block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Renderer extends \Magento\Weee\Block\Item\Price\Renderer
{

    /**
     * Get original price
     *
     * @return float
     */
    public function getOriginPrice()
    {
        $specialPrice = $this->getItem()->getProduct()->getSpecialPrice();
        //Check is reduce price
        if($specialPrice == ""){
            return;
        }
        //End

        $priceOrigin = $this->getItem()->getProduct()->getPrice();

        //Get object dependence
        $objectManger = \Magento\Framework\App\ObjectManager::getInstance();
        $factoryCurrency = $objectManger->get("Magento\Directory\Model\Currency");
        $priceInFormat = $factoryCurrency->formatPrecision($priceOrigin,0,[],false,false);
        return $priceInFormat;
    }

    /**
     * Get discount percentage
     *
     * @return float
     */
    public function getDiscountPercentage()
    {
        $specialPrice = $this->getItem()->getProduct()->getSpecialPrice();
        //Check is reduce price
        if($specialPrice == ""){
            return;
        }
        //End

        $priceOrigin = $this->getItem()->getProduct()->getPrice();
        $discountPercentage = 100 - round(($specialPrice / $priceOrigin)*100);
        return "-" . $discountPercentage . "%";
    }
   

}
