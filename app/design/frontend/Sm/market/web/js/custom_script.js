jQuery(function(){
	/* hover on dropdown menu */
	hoverDropdown();

	/* popup */
	fnClosePopupOnEven();

	/* tabs */
	fnChangeTab();

	/* collapse & expand */
	fnCollapseExpand();

	/* change qty input on checkout page */
	fnQtyInputCheckout();

	/* active slider */
	activeSlider();

	/* set full screen width */
	setFullScreenWidth();

	/* upload file */
	fnUploadFileBtn();
	fnGetUploadFileName();
});

/**************** dropdown menu ****************/
function hoverDropdown(){
	jQuery(".dropdown-cus-title, .dropdown-cus, .tooltip-cus").hover(
		function(){
			if(jQuery(this).parent().find(".tooltip-cus").length > 0){
				jQuery(this).parent().find(".tooltip-cus").show();
			} else {
				jQuery(this).parent().find(".dropdown-cus").show();	
			}
			
		}, function(){
			if(jQuery(this).parent().find(".tooltip-cus").length > 0){
				jQuery(this).parent().find(".tooltip-cus").hide();
			} else {
				jQuery(this).parent().find(".dropdown-cus").hide();
			}
		}
	);
}

/**************** popup ****************/
function fnShowPopup(popId){
	jQuery("#" + popId ).show();
}

function fnClosePopup(){
	jQuery(".layer-popup").hide();
}

function fnClosePopupOnEven(){
	jQuery(document).on("click",function(event){
		if(event.target.className.indexOf("layer-popup")>-1){
			fnClosePopup();
		}
	});

	jQuery(document).on("keydown",function(event){
		if(event.keyCode == 27){
			fnClosePopup();
		}
	});
}

/**************** tabs ****************/
function fnChangeTab(){
	jQuery(document).on("click",".tabs-cus .tab-cus",function(){
		var tabCntId = jQuery(this).parent().attr("tab-content-id");
		jQuery(this).parent().find(".tab-cus").removeClass("on");
		jQuery(this).addClass("on");
		var idx = jQuery(this).index();
		jQuery("#"+tabCntId).eq(0).find(".tab-cus-cnt").removeClass("on");
		jQuery("#"+tabCntId).eq(0).find(".tab-cus-cnt").eq(idx).addClass("on");
	});
}

/**************** collapse & expand ****************/
function fnCollapseExpand() {
	jQuery(document).on("click",".collapse-expands .collapse-expands-title",function(){
		jQuery(this).toggleClass("active");
		jQuery(this).parent().find(".collapse-expands-cnt").slideToggle();
	});
}

/**************** change qty input on checkout page ****************/
function fnQtyInputCheckout(){
	jQuery(document).on("click",'.quantity-plus',function () {
        var input = jQuery(this).parent().find('.input-text.qty');
        input.val(Number(input.val())+1);
    });
    
    jQuery(document).on("click",'.quantity-minus',function () {
        var input = jQuery(this).parent().find('.input-text.qty');
        var value = Number(input.val())-1;
        if(value > 0){
            input.val(value);
        }
    });	
}

/**************** slider ****************/
function activeSlider(){
	var clients_slider = jQuery(".clients-say-slider");
	clients_slider.owlCarousel({
		nav: true,
		autoplay: true,
		autoplayHoverPause: true,
		loop: true,
		responsive: {
			0: {items:1},
			480: {items:1},
			768: {items:1},
			991: {items:1},
			1200: {items:1}
		}
	});
}

/**************** set full screen width ****************/
function setFullScreenWidth(){
	var w = jQuery(window).width();
	jQuery(".fullwidth").width(w);
	jQuery(".fullwidth-cnt").width(w);
	
	if(jQuery(".fullwidth").length>0){
		var marLeft = jQuery(".fullwidth > .container").css("margin-left");
		if(marLeft != "undifined" && marLeft.indexOf("px")>-1){
			marLeft = marLeft.replace(/px/g,"")*(-1);
			jQuery(".fullwidth").css({"margin-left":marLeft});
			jQuery(".fullwidth-cnt").css({"margin-left":marLeft});
		}	
	}
}


/**************** upload file ****************/
function fnUploadFileBtn() {
	jQuery(document).on("click",".upload-file-btn",function(){
		jQuery(this).parent().find("input[type='file']").click();
	});
}

function fnGetUploadFileName() {
	jQuery(document).on("change","input[type='file']",function(){
		if (this.files[0]) {
			jQuery(this).parent().find(".file-name").text(this.files[0].name);
		} else {
			jQuery(this).parent().find(".file-name").text("");
		}
		
	});
}

function fnValidateUploadFile() {
	var isRightExtension;
	var isRightSize;
	jQuery("input[type='file']").each(function(i,e){
		var extensionArr = jQuery(this).attr("file-extension").toLowerCase().split(",");
		var file = this.files[0];
		var extensionChosen = file.name.split(".");
		extensionChosen = extensionChosen[extensionChosen.length-1].toLowerCase();
		isRightExtension = false;
		jQuery(extensionArr).each(function(i,e){
			if(extensionChosen.toLowerCase().trim() == this.trim()){
				isRightExtension = true;
				return false;
			}
		})

		if(!isRightExtension) {
			alert("File [" +file.name+ "] is not one of these extension: " + extensionArr);
			return false;
		}

		isRightSize = true;
		if (file.size > 2097152) {
			alert("File [" +file.name+ "] is over 2MB. Please choose another file");
			isRightSize = false;
			return false;
		}
	});
	return (isRightExtension && isRightSize);
}
